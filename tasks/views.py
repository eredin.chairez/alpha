from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.models import Project
from tasks.forms import TaskForm
# Create your views here.

@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            recipe = form.save()
            recipe.author = request.user
            recipe.save()
            return redirect("projects_list")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def my_projects_list(request):
    projects = Project.objects.filter(assignee=request.user)
    context = {
        "projects_list": projects,
    }
    return render(request, "projects/list.html", context)
