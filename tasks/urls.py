from django.urls import path
from tasks.views import create_task, my_projects_list


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", my_projects_list, name="show_my_tasks")
]
